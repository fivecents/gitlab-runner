resource "gitlab_project_variable" "inventory" {
  project       = local.config.gitlab_project_id
  key           = "ansible_inventory"
  value         = local_file.inventory.content
  protected     = false
  variable_type = "file"
}