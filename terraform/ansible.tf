resource "local_file" "inventory" {
  content = templatefile("templates/inventory.tpl",
    {
      app_host = vultr_instance.app_host[*],
      config   = local.config,
    }
  )
  filename = "../inventory"
}