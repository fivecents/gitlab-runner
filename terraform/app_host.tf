resource "vultr_instance" "app_host" {
  count       = local.config.app_count
  region      = local.config.region
  plan        = local.config.app_plan
  os_id       = local.config.image_id
  ssh_key_ids = [local.config.ssh_key_id]
  hostname    = "${local.config.app_name}-${count.index}"
  label       = "${local.config.app_name}-${count.index}"
  tags        = local.config.tags
  vpc_ids     = [local.config.vpc_id]
  lifecycle {
    ignore_changes = [
      ssh_key_ids,
    ]
  }
}