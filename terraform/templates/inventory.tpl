[all:children]
${config.app_name}_app

[${config.app_name}_app:vars]
server_role = application 

[${config.app_name}_app]
%{ for instance in app_host ~}
${instance.main_ip}
%{ endfor ~}