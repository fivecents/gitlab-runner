terraform {
  backend "http" {}
  required_providers {
    vultr = {
      source  = "vultr/vultr"
      version = "2.17.1"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.4.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.6.0"
    }
  }
}